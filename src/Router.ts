import * as http from "http";

export type Handler<T> = (
  request: http.IncomingMessage & { body?: T },
  response: http.ServerResponse
) => void;

export class Router<T> {
  private handlers: { [key: string]: Handler<T> } = {};

  private addHandler(method: string, path: string, handler: Handler<T>): void {
    const endpoint = `${method}:${path}`;
    if (this.handlers[endpoint]) {
      throw new Error(`Handler already specified for ${endpoint}`);
    }
    this.handlers[endpoint] = handler;
  }

  get(path: string, handler: Handler<T>): this {
    this.addHandler("GET", path, handler);
    return this;
  }

  post(path: string, handler: Handler<T>): this {
    this.addHandler("POST", path, handler);
    return this;
  }

  put(path: string, handler: Handler<T>): this {
    this.addHandler("PUT", path, handler);
    return this;
  }

  delete(path: string, handler: Handler<T>): this {
    this.addHandler("DELETE", path, handler);
    return this;
  }

  getHandler(): Handler<T> {
    return (
      request: http.IncomingMessage & { body?: any },
      response: http.ServerResponse
    ) => {
      let body = "";

      request.on("data", (chunk) => {
        body += chunk.toString();
      });

      request.on("end", () => {
        request.body = JSON.parse(body);

        const method = request.method?.toUpperCase();
        const path = request.url;
        if (!method || !path) {
          response.writeHead(400);
          response.end();
          return;
        }

        const handler = this.handlers[`${method}:${path}`];
        if (handler) {
          try {
            return handler(request, response);
          } catch (error) {
            // console.error(error);
            response.statusCode = 500;
            return response.end();
          }
        } else {
          response.statusCode = 404;
          return response.end();
        }
      });
    };
  }
}

export enum HTTPMethod {
  GET = "GET",
  POST = "POST",
  PUT = "PUT",
  DELETE = "DELETE",
}
